package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.openqa.selenium.By.id;
import static org.openqa.selenium.By.xpath;

public class GMailLoginPage extends BasePage {

    private static final String HOME_PAGE_VIEW_CONTAINER = "view_container";
    private static final String USERNAME_FIELD = "//input[@type='email']";
    private static final String PASSWORD_FIELD = "//div[@id='password']//input";
    private static final String LOGIN_NEXT_BUTTON = "//div[@id='identifierNext']//button[@jsname='LgbsSe']";
    private static final String PASSWORD_NEXT_BUTTON = "//div[@id='passwordNext']//button[@jsname='LgbsSe']";



    public GMailLoginPage(WebDriver driver) {
        super(driver);
    }

    public boolean isHomePageViewContainerIsVisible() {
        return driver.findElement(id(HOME_PAGE_VIEW_CONTAINER)).isDisplayed();
    }

    public void enterTextIntoUsernameField(final String textToEnter) {
        driver.findElement(xpath(USERNAME_FIELD)).sendKeys(textToEnter);
    }

    public void enterTextIntoPasswordField(final String textToEnter) {
        driver.findElement(xpath(PASSWORD_FIELD)).sendKeys(textToEnter);
    }

    public WebElement getPasswordField() {
        return driver.findElement(xpath(PASSWORD_FIELD));
    }

    public void clickOnLoginNextButton() {
        driver.findElement(xpath(LOGIN_NEXT_BUTTON)).click();
    }

    public void clickOnPasswordNextButton() {
        driver.findElement(xpath(PASSWORD_NEXT_BUTTON)).click();
    }

}
