package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class BasePage {

    WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public void waitVisibilityOfElement(long secondsToWait, WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(secondsToWait));
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void waitForPageLoadComplete(long secondsToWait) {
        new WebDriverWait(driver, Duration.ofSeconds(secondsToWait)).until(
                webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
    }

    public void implicitWait(long secondsToWait) {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(secondsToWait));
    }
}
