package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.openqa.selenium.By.*;

public class GMailInboxPage extends BasePage {

    private static final String ACCOUNT_IMG = "div[class='gb_Ma gb_gd gb_lg gb_f gb_zf']";
    private static final String WRITE_BUTTON = "//div[@class='T-I T-I-KE L3']";
    private static final String NEW_MAIL_POPUP = "nH Hd";
    private static final String RECIPIENT_FIELD = "//input[@class='agP aFw']";
    private static final String MAIL_TEXT_FIELD = "//div[contains(@class,'Am Al editable')]";
    private static final String SEND_BUTTON = "//div[contains(@class,'T-I J-J5-Ji aoO')]";
    private static final String SENT_BUTTON = "//span/a[contains(@href,'sent')]";
    private static final String SENT_MAILS = "//tr[@class='zA yO']";

    public GMailInboxPage(WebDriver driver) {
        super(driver);
    }

    public boolean isAccountIMGVisible() {
        return driver.findElement(cssSelector(ACCOUNT_IMG)).isDisplayed();
    }

    public void clickOnWriteButton() {
        driver.findElement(xpath(WRITE_BUTTON)).click();
    }

    public boolean isNewMailPopupVisible() {
        return driver.findElement(className(NEW_MAIL_POPUP)).isDisplayed();
    }

    public void enterTextToRecipientField(final String text) {
        driver.findElement(xpath(RECIPIENT_FIELD)).sendKeys(text);
    }

    public void enterTextToMailField(final String text) {
        driver.findElement(xpath(MAIL_TEXT_FIELD)).sendKeys(text);
    }

    public void clickOnSendButton() {
        driver.findElement(xpath(SEND_BUTTON)).click();
    }

    public void clickOnSentButton() {
        driver.findElement(xpath(SENT_BUTTON)).click();
    }

    public WebElement getSentMails() {
        return driver.findElement(xpath(SENT_MAILS));
    }

    public boolean isSentMailsVisible() {
        return driver.findElement(xpath(SENT_MAILS)).isDisplayed();
    }
}
