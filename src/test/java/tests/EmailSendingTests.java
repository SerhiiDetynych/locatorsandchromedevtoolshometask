package tests;

import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class EmailSendingTests extends BaseTest {

    private static final long TIME_TO_WAIT = 5;

    private static final String GMAIL_LOGIN = "labawebdrivertest@gmail.com";
    private static final String PASSWORD = "Qazwsxedcrfvtgb12345";
    private static final String EMAIL_TO_SEND = "labawebdrivertest@meta.ua";
    private static final String MAIL_TEXT = "Hello, my name Google!";


    @Test
    public void checkThatMailWillSend() {
        getGMailLoginPage().waitForPageLoadComplete(TIME_TO_WAIT);
        assertTrue(getGMailLoginPage().isHomePageViewContainerIsVisible());
        getGMailLoginPage().enterTextIntoUsernameField(GMAIL_LOGIN);
        getGMailLoginPage().clickOnLoginNextButton();
        getGMailLoginPage().implicitWait(TIME_TO_WAIT);
        getGMailLoginPage().waitVisibilityOfElement(TIME_TO_WAIT, getGMailLoginPage().getPasswordField());
        getGMailLoginPage().enterTextIntoPasswordField(PASSWORD);
        getGMailLoginPage().clickOnPasswordNextButton();
        getGMailInboxPage().waitForPageLoadComplete(TIME_TO_WAIT);
        assertTrue(getGMailInboxPage().isAccountIMGVisible());
        getGMailInboxPage().clickOnWriteButton();
        getGMailInboxPage().implicitWait(TIME_TO_WAIT);
        assertTrue(getGMailInboxPage().isNewMailPopupVisible());
        getGMailInboxPage().enterTextToRecipientField(EMAIL_TO_SEND);
        getGMailInboxPage().enterTextToMailField(MAIL_TEXT);
        getGMailInboxPage().clickOnSendButton();
        getGMailInboxPage().clickOnSentButton();
        getGMailInboxPage().waitForPageLoadComplete(TIME_TO_WAIT);
        getGMailInboxPage().waitVisibilityOfElement(TIME_TO_WAIT, getGMailInboxPage().getSentMails());
        assertTrue(getGMailInboxPage().isSentMailsVisible());
    }
}
